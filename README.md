# GitLab Aquaponic System Prototype Project

This GitLab project is centered around an aquaponic system prototype for commercial use, created using SOLIDWORKS 2022. The project was developed within the context of a design thinking workshop and went through various stages, including Observation, Emphasize, Ideation, and Implementation & Prototyping. This GitLab repository specifically focuses on the application of the prototyping phase within the workshop.

## Project Overview

The aquaponic system prototype is designed as a 3D model using SOLIDWORKS 2022. It's intended for commercial use and consists of several essential components, each of which plays a crucial role in the overall functionality of the system. The following objects are included in the 3D model:

1. **Biofiltration**: This component is vital for maintaining water quality and providing a habitat for beneficial bacteria.

2. **Recycle Pipe**: The recycle pipe is responsible for circulating water within the aquaponic system.

3. **Pipe Sets x2**: Two sets of pipes for transporting water and other substances within the system.

4. **Waste Storage**: This container is designed to store waste materials produced by the aquaponic system.

5. **Well**: The well serves as a water reservoir and a central point for the aquaponic system's water management.

6. **FishTank x2**: There are two fish tanks in the system, which are home to the fish that play a crucial role in the aquaponic ecosystem.

7. **Radial Flow Filter (RFF) x2**: These filters help in removing solid waste and debris from the water.

8. **RFF Lid x2**: Lids for the radial flow filters, ensuring efficient filtration.

9. **Grow Media x5**: Five sets of grow media for nurturing plants as part of the aquaponic system.
